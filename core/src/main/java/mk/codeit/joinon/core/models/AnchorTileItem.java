package mk.codeit.joinon.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;

import javax.inject.Inject;

@Model(adaptables = Resource.class)
public class AnchorTileItem {

    @Inject
    protected String text;

    @Inject
    protected String icon;

    public String getText() {
        return text;
    }

    public String getIcon() {
        return icon;
    }
}
