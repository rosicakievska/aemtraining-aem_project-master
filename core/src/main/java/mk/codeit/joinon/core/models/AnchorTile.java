package mk.codeit.joinon.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;

@Model(adaptables = Resource.class)
public class AnchorTile {

    @Self
    protected Resource resource;

    @Inject
    @Default(values = "Default title")
    protected String title;

    protected List<AnchorTileItem> listOfItems = new LinkedList<>();

    @PostConstruct
    void init() {
        if(resource != null && resource.getChild("items") != null) {
            resource.getChild("items").getChildren().forEach(item -> {
                AnchorTileItem anchorTileItem = item.adaptTo(AnchorTileItem.class);
                if(anchorTileItem != null) {
                    listOfItems.add(anchorTileItem);
                }
            });
        }
    }

    public String getTitle() {
        return title;
    }

    public List<AnchorTileItem> getListOfItems() {
        return listOfItems;
    }
}
