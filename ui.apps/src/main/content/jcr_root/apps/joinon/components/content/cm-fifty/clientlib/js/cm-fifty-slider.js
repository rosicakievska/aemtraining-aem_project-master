var swiper = new Swiper('.cm-fifty-slider.cm-fifty.grey-dark-bg.swiper-container', {
    on: {
        init: function() {}
    },
    spaceBetween: 30,
    autoplay: {
        delay: 4000,
        disableOnInteraction: false
    },
    pagination: {
        el: '.swiper-pagination',
        clickable: true
    },
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    }
});


/* SCROLL PANDA TO CONTACT FORM */

$(".contact-scroll").click(function() {
    var headerHeight = (screen.width >= 1024) ? 100 : 58;

    $([document.documentElement, document.body]).animate({
        scrollTop: $(".cm-form").offset().top - headerHeight
    }, 2000);
});

$(".contact-close").click(function() {
    $('.contact').fadeOut();
});