$(".login-form").submit(function(e) {
    e.preventDefault();
});

var controllo = true;

$(document).on("click", "form .accedi-button", function() {

    var $form = $(this).closest('form'),
        $user = $form.find('[name="user"]'),
        $pass = $form.find('[name="password"]');

    if ($user.val().trim().length === 0) {
        controllo = false;
        $user.addClass('error');
        $('.login-message-error').show();
    } else {
        controllo = true;
        $user.removeClass('error');
    }

    if ($pass.val().trim().length === 0) {
        controllo = false;
        $pass.addClass('error');
        $('.login-message-error').show();
    } else {
        controllo = !controllo ? false : true;
        $pass.removeClass('error');
    }

    if (controllo == true) {
        $('.login-message-error').hide();
        console.log('MANAGE FORM SUBMIT HERE');
    }

});


// HEADER MOBILE

$(document).on("click", ".navigation-icon", function() {
    $('#mobileMenu').addClass('id-show');
    $('.bg-overlay').fadeIn();
    $('body').css('overflow', 'hidden');
});

$(document).on("click", ".close-button", function() {
    $('.main-mobile-content').find('.id-show').removeClass('id-show');
    $('.bg-overlay').fadeOut();
    $('body').css('overflow', 'auto');
});

$(document).on("click", ".applicazioni", function() {
    $('#applicazioni').addClass('id-show');
});

$(document).on("click", ".prodotti", function() {
    $('#prodotti').addClass('id-show');
});

$(document).on("click", ".emobility", function() {
    $('#emobility').addClass('id-show');
});

$(document).on("click", ".menu1", function() {
    $('#menu1').addClass('id-show');
});

$(document).on("click", ".menu2", function() {
    $('#menu2').addClass('id-show');
});

$(document).on("click", ".menu3", function() {
    $('#menu3').addClass('id-show');
});

$(document).on("click", ".menu4", function() {
    $('#menu4').addClass('id-show');
});

$(document).on("click", ".menu5", function() {
    $('#menu5').addClass('id-show');
});

$(document).on("click", ".menu6", function() {
    $('#menu6').addClass('id-show');
});

$(window).on('resize', function() {
    var win = $(this);
    if (win.width() > 1024) {
        $('.main-mobile-content').find('.id-show').removeClass('id-show');
        $('.bg-overlay').fadeOut();
        $('body').css('overflow', 'auto');
    }
});

function slideLeft($scope) {
    $scope.closest('.menu-mobile-container').removeClass('id-show');
}